<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once 'config.php';

// Função para registrar logs de erro em um arquivo
function registrarErro($mensagem) {
    // Caminho do arquivo de log
    $caminhoArquivo = 'logs/errosMysql.txt';

    // Mensagem formatada com data/hora
    $mensagemFormatada = "[" . date('Y-m-d H:i:s') . "] " . $mensagem . "\n";

    // Adiciona a mensagem ao arquivo de log
    file_put_contents($caminhoArquivo, $mensagemFormatada, FILE_APPEND | LOCK_EX);
}

try{

    $con = mysqli_connect( DDBB_HOST,  DDBB_USER,  DDBB_PASSWORD,  DDBB_NAME);

    $sql = "SELECT * FROM teste";

    $result = mysqli_query($con, $sql);

    http_response_code(200);
    $outp='';
    $virg = '';
    $outp .= '[';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $outp .= $virg;
        $outp .= '{"id":"'. $row['id'].'",';
        $outp .= '"email":"'. $row['email'].'",';
        $outp .= '"nombre":"'. $row['nombre'].'",';
        $outp .= '"apellido":"'. $row['apellido'].'",';
        $outp .= '"rol":"'. $row['rol'].'",';
        $outp .= '"provincia":"'. $row['provincia'].'",';
        $outp .= '"municipio":"'. $row['municipio'].'"}';
        $virg = ',';
    }
    $outp .= ']';
    echo $outp;

} catch (Exception $e) {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "An error occurred"));
    registrarErro('Erro: ' . $e->getMessage());
}

// Fechar a conexão
mysqli_close($con);

?>