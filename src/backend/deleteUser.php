<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once 'config.php';

// Função para registrar logs de erro em um arquivo
function registrarErro($mensagem) {
    // Caminho do arquivo de log
    $caminhoArquivo = 'logs/errosMysql.txt';

    // Mensagem formatada com data/hora
    $mensagemFormatada = "[" . date('Y-m-d H:i:s') . "] " . $mensagem . "\n";

    // Adiciona a mensagem ao arquivo de log
    file_put_contents($caminhoArquivo, $mensagemFormatada, FILE_APPEND | LOCK_EX);
}

// Decodificar os dados JSON recebidos
$data = json_decode(file_get_contents("php://input"));

// Verificar se o ID foi passado corretamente
if (!isset($data)) {
    http_response_code(400); // Bad request
    echo json_encode(array("message" => "ID not provided"));
    exit;
}

// Atribuir o ID recebido
$id = $data;

$con = mysqli_connect( DDBB_HOST,  DDBB_USER,  DDBB_PASSWORD,  DDBB_NAME);

// Verificar a conexão
if (mysqli_connect_errno()) {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "Failed to connect to database"));
    registrarErro('Erro: ' . "Failed to connect to database");
    exit;
}

try {
    // Preparar a consulta SQL com um espaço reservado para o valor do ID
    $sql = "DELETE FROM teste WHERE id = ?";

    // Preparar a declaração
    if ($stmt = mysqli_prepare($con, $sql)) {
        // Vincular a variável à declaração como parâmetro
        mysqli_stmt_bind_param($stmt, "i", $id);

        // Executar a declaração
        mysqli_stmt_execute($stmt);

        // Verificar se a exclusão foi bem-sucedida
        if (mysqli_stmt_affected_rows($stmt) > 0) {
            http_response_code(200); // OK
            echo json_encode(array("message" => "Event deleted successfully"));
        } else {
            http_response_code(404); // Not found
            echo json_encode(array("message" => "Event not found"));
        }

        // Fechar a declaração
        mysqli_stmt_close($stmt);
    } else {
        throw new Exception("Failed to prepare SQL statement");
    }
} catch (Exception $e) {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "An error occurred"));
    registrarErro('Erro: ' . $e->getMessage());
}

// Fechar a conexão
mysqli_close($con);

?>
