<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once 'config.php';

require_once 'lib/php-jwt/src/JWTExceptionWithPayloadInterface.php';
require_once 'lib/php-jwt/src/BeforeValidException.php';
require_once 'lib/php-jwt/src/ExpiredException.php';
require_once 'lib/php-jwt/src/SignatureInvalidException.php';
require_once 'lib/php-jwt/src/JWT.php';
require_once 'lib/php-jwt/src/JWK.php';

// Função para registrar logs de erro em um arquivo
function registrarErro($mensagem) {
    // Caminho do arquivo de log
    $caminhoArquivo = 'logs/errosMysql.txt';

    // Mensagem formatada com data/hora
    $mensagemFormatada = "[" . date('Y-m-d H:i:s') . "] " . $mensagem . "\n";

    // Adiciona a mensagem ao arquivo de log
    file_put_contents($caminhoArquivo, $mensagemFormatada, FILE_APPEND | LOCK_EX);
}

use \Firebase\JWT\JWT;

// Sua chave secreta
$key = SECRET_KEY;

$userData = null;

$data = json_decode(file_get_contents("php://input"));

$email = $data->email;
$contrasena = $data->contrasena;

$con = mysqli_connect( DDBB_HOST,  DDBB_USER,  DDBB_PASSWORD,  DDBB_NAME);

// Verificar a conexão
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    registrarErro('Erro: ' .  "Failed to connect to MySQL");
    exit();
}

// Preparar a consulta SQL com um espaço reservado para o valor do e-mail
$sql = "SELECT * FROM teste WHERE email = ? AND contrasena = ?";

// Preparar a declaração
if ($stmt = mysqli_prepare($con, $sql)) {
    // Vincular variáveis ​​à declaração como parâmetros
    mysqli_stmt_bind_param($stmt, "ss", $email, $contrasena);

    // Executar a declaração
    mysqli_stmt_execute($stmt);

    // Armazenar o resultado
    $result = mysqli_stmt_get_result($stmt);

    // Verificar se há resultados
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        // Dados do usuário
        $userData = [
            'email' => $row['email'],
            'nombre' => $row['nombre'],
            'apellido' => $row['apellido'],
            'id' => $row['id'],
            'rol' => $row['rol'],
            'provincia' => $row['provincia'],
            'municipio' => $row['municipio'],
        ];


        // Configurar o payload do token
        $tokenPayload = [
            'iat' => time(),
            'exp' => time() + 3600,
            'data' => $userData,
        ];

        // Gerar o token
        $token = JWT::encode($tokenPayload, $key, 'HS256');

        // Retornar o token para o front-end
        echo json_encode(['token' => $token]);
    } else {
        registrarErro('Erro: ' .  "Failed to load userData");
        // Retornar o token para o front-end
        echo 'error';

    }

    // Fechar a declaração
    mysqli_stmt_close($stmt);
}

// Fechar a conexão
mysqli_close($con);


?>