<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: acess");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once 'config.php';

// Função para registrar logs de erro em um arquivo
function registrarErro($mensagem) {
    // Caminho do arquivo de log
    $caminhoArquivo = 'logs/errosMysql.txt';

    // Mensagem formatada com data/hora
    $mensagemFormatada = "[" . date('Y-m-d H:i:s') . "] " . $mensagem . "\n";

    // Adiciona a mensagem ao arquivo de log
    file_put_contents($caminhoArquivo, $mensagemFormatada, FILE_APPEND | LOCK_EX);
}


$data = json_decode(file_get_contents("php://input"));

$id = $data->id;
$nombre = $data->nombre;
$apellido = $data->apellido;
$email = $data->email;
$provincia = $data->provincia;
$municipio = $data->municipio;
$contrasena = $data->contrasena;
$rol = $data->rol;

try{

    $con = mysqli_connect( DDBB_HOST,  DDBB_USER,  DDBB_PASSWORD,  DDBB_NAME);

    $sql = "UPDATE teste SET ";

    $virg = '';

    if($nombre != ''){
        $sql .= "nombre = '". $nombre ."' ";
        $virg = ',';

    }
    if($apellido != ''){
        $sql .= $virg."apellido = '". $apellido ."' ";
        $virg = ',';
    }
    if($email != ''){
        $sql .= $virg."email = '". $email ."' ";
        $virg = ',';
    }

    if($provincia != ''){
        $sql .= $virg."provincia = '". $provincia ."' ";
        $virg = ',';
    }
    if($municipio != ''){
        $sql .= $virg."municipio = '". $municipio ."' ";
        $virg = ',';
    }
    if($contrasena != ''){
        $sql .= $virg."contrasena = '". $contrasena ."' ";
        $virg = ',';
    }

    if($rol != ''){
        $sql .= $virg."rol = ". $rol ." ";
    }




    $sql .= "WHERE id = ". $id;

    $result = mysqli_query($con, $sql);


    echo json_encode($result);
} catch (Exception $e) {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "An error occurred"));
    registrarErro('Erro: ' . $e->getMessage());
}

// Fechar a conexão
mysqli_close($con);


?>