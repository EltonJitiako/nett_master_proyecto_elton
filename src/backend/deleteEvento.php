<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once 'config.php';

// Decodificar os dados JSON recebidos
$data = json_decode(file_get_contents("php://input"));

// Verificar se o ID foi passado corretamente
if (!isset($data)) {
    http_response_code(400); // Bad request
    echo json_encode(array("message" => "ID not provided"));
    exit;
}

// Atribuir o ID recebido
$id = $data;

$con = mysqli_connect( DDBB_HOST,  DDBB_USER,  DDBB_PASSWORD,  DDBB_NAME);

// Verificar a conexão
if (mysqli_connect_errno()) {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "Failed to connect to database"));
    exit;
}

// Preparar a consulta SQL com um espaço reservado para o valor do ID
$sql = "DELETE FROM eventos WHERE id_evento = ?";

// Preparar a declaração
if ($stmt = mysqli_prepare($con, $sql)) {
    // Vincular a variável à declaração como parâmetro
    mysqli_stmt_bind_param($stmt, "i", $id);

    // Executar a declaração
    mysqli_stmt_execute($stmt);

    // Verificar se a exclusão foi bem-sucedida
    if (mysqli_stmt_affected_rows($stmt) > 0) {
        http_response_code(200); // OK
        echo json_encode(array("message" => "Event deleted successfully"));
    } else {
        http_response_code(404); // Not found
        echo json_encode(array("message" => "Event not found"));
    }

    // Fechar a declaração
    mysqli_stmt_close($stmt);
} else {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "Failed to prepare SQL statement"));
}

// Fechar a conexão
mysqli_close($con);
?>

