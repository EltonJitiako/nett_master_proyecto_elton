<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// Função para registrar logs de erro em um arquivo
function registrarErro($mensagem) {
    // Caminho do arquivo de log
    $caminhoArquivo = 'logs/errosMysql.txt';

    // Mensagem formatada com data/hora
    $mensagemFormatada = "[" . date('Y-m-d H:i:s') . "] " . $mensagem . "\n";

    // Adiciona a mensagem ao arquivo de log
    file_put_contents($caminhoArquivo, $mensagemFormatada, FILE_APPEND | LOCK_EX);
}


$data = json_decode(file_get_contents("php://input"));

$id = $data;

require_once 'config.php';

try{
    $con = mysqli_connect( DDBB_HOST,  DDBB_USER,  DDBB_PASSWORD,  DDBB_NAME);

    $sql = "SELECT * FROM eventos WHERE id_evento = ". $id;

    $result = mysqli_query($con, $sql);

    //echo 'testessssssssss' .$result;
    http_response_code(200);
    $outp='';
    $virg = '';
    $outp .= '[';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $outp .= $virg;
        $outp .= '{"id":"'. $row['id'].'",';
        $outp .= '"texto":"'. $row['texto'].'",';
        $outp .= '"date_evento":"'. $row['date_evento'].'",';
        $outp .= '"time_evento":"'. $row['time_evento'].'"}';
        $virg = ',';
    }
    $outp .= ']';
    echo $outp;
} catch (Exception $e) {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "An error occurred"));
    registrarErro('Erro: ' . $e->getMessage());
}

// Fechar a conexão
mysqli_close($con);

?>