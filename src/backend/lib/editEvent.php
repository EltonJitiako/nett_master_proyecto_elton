<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: acess");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// Função para registrar logs de erro em um arquivo
function registrarErro($mensagem) {
    // Caminho do arquivo de log
    $caminhoArquivo = 'logs/errosMysql.txt';

    // Mensagem formatada com data/hora
    $mensagemFormatada = "[" . date('Y-m-d H:i:s') . "] " . $mensagem . "\n";

    // Adiciona a mensagem ao arquivo de log
    file_put_contents($caminhoArquivo, $mensagemFormatada, FILE_APPEND | LOCK_EX);
}


$data = json_decode(file_get_contents("php://input"));

$id_evento = $data->id_evento;
$date_evento = $data->date_evento;
$time_evento = $data->time_evento;
$texto = $data->texto;


require_once 'config.php';

try {
    $con = mysqli_connect( DDBB_HOST,  DDBB_USER,  DDBB_PASSWORD,  DDBB_NAME);

    $sql = "UPDATE eventos SET ";

    $virg = '';

    if($date_evento != ''){
        $sql .= "date_evento = '". $date_evento ."' ";
        $virg = ',';

    }
    if($time_evento != ''){
        $sql .= $virg."time_evento = '". $time_evento ."' ";
        $virg = ',';
    }

    if($texto != ''){
        $sql .= $virg."texto = '". $texto ."' ";
    }




    $sql .= "WHERE id_evento = ". $id_evento;

    $result = mysqli_query($con, $sql);


    echo json_encode($result);
} catch (Exception $e) {
    http_response_code(500); // Internal server error
    echo json_encode(array("message" => "An error occurred"));
    registrarErro('Erro: ' . $e->getMessage());
}

// Fechar a conexão
mysqli_close($con);


?>