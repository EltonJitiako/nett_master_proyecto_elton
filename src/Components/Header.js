import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import logo from '../img/logo.png';
import { jwtDecode } from 'jwt-decode';


const Header=()=>{

    /********* Variables  *********/

    const token = localStorage.getItem('token');

    const Logout=()=>{
        localStorage.removeItem('token');
        localStorage.clear();
    }

    if(token === null) {
        return(
            <Navbar class="caixa3" expand="lg" className="bg-body-tertiary" bg="dark" data-bs-theme="dark">
                <Container id="header_footer_border1">
                    <Navbar.Brand href="#"><Link class="nav-link navbar-brand" to='/'><img class="logo" src={logo} alt="logo"/></Link></Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '150px' }}
                        navbarScroll
                    >
                        <Nav.Link href="#/register">REGISTRO</Nav.Link>

                        <Nav.Link href="#/login">LOGIN</Nav.Link>
                        <NavDropdown.Divider />  
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        )
    }  else {
        const decodeHeader = jwtDecode(token);
        if(decodeHeader.data.rol === 0) {
            return(
                <Navbar expand="lg" className="bg-body-tertiary" bg="dark" data-bs-theme="dark">
                    <Container id="header_footer_border1">
                        <Navbar.Brand href="#"><Link class="nav-link navbar-brand" to='/'><img class="logo" src={logo} alt="logo"/></Link></Navbar.Brand>
                        <Navbar.Toggle aria-controls="navbarScroll" />
                        <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="me-auto my-2 my-lg-0"
                            style={{ maxHeight: '150px' }}
                            navbarScroll
                        >
                            <Nav.Link href="#/calendar">CALENDARIO</Nav.Link>
                            <Nav.Link href="#/events">EVENTOS</Nav.Link>
                            <Nav.Link href="#/perfil">PERFIL</Nav.Link>
                            <Nav.Link href="#/login" onClick={Logout}><div class="logout">LOGOUT</div></Nav.Link>  
                            <NavDropdown.Divider />  
                        </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            )
        } else if(decodeHeader.data.rol === 1) {
            return(
                <Navbar expand="lg" className="bg-body-tertiary" bg="dark" data-bs-theme="dark">
                    <Container id="header_footer_border1">
                        <Navbar.Brand href="#"><Link class="nav-link navbar-brand" to='/'><img class="logo" src={logo} alt="logo"/></Link></Navbar.Brand>
                        <Navbar.Toggle aria-controls="navbarScroll" />
                        <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="me-auto my-2 my-lg-0"
                            style={{ maxHeight: '150px' }}
                            navbarScroll
                        >
                            <Nav.Link href="#/calendar">CALENDARIO</Nav.Link>
                            <Nav.Link href="#/events">EVENTOS</Nav.Link>
                            <Nav.Link href="#/perfil">PERFIL</Nav.Link>
                            <Nav.Link href="#/login" onClick={Logout}><div class="logout">LOGOUT</div></Nav.Link>
                            <Nav.Link href="#/adm">ADMINISTRACION</Nav.Link>  
                            <NavDropdown.Divider />  
                        </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            )
        }
    }
}

export default Header;