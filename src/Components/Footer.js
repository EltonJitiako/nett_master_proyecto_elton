
import { Link } from 'react-router-dom';

const Footer=()=>{

    return (
        <div class="footer">
          <link rel="preconnect" href="https://fonts.googleapis.com"></link>
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin></link>
          <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100..900;1,100..900&family=Playfair+Display:ital,wght@0,400..900;1,400..900&display=swap" rel="stylesheet"></link>
          <section class="bg-dark">
            <footer class="text-white">
              <div class="d-flex justify-content-between">
                      <div class="p-2 bg-dark" id="header_footer_border1">
                        © {Intl.DateTimeFormat('pt-BR', { year: 'numeric' }).format(Date.now())} 
                        &nbsp;CALENDARFREE365
                      </div>
                      <div class="p-2 bg-dark" id="header_footer_border2">
                        <a href="https://eltonjitiako.github.io/letscodebegins/" class="text-white" style={{ textDecoration: "none", outline: "none" }}>by Elton Jitiako</a>
                      </div>
              </div>
              <div class="text-center">
                <div class="bg-dark">
                  <Link class="nav-link navbar-brand" to='/privacookies'>Privacidad y Cookies</Link>
                </div>
              </div>
            </footer>
          </section>
        </div>
    )
}

export default Footer;


