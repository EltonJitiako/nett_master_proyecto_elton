import { useEffect } from 'react';
import { useState } from 'react';
import axios from 'axios';
import Carousel from 'react-bootstrap/Carousel';
import Header from '../Components/Header';
import Table from 'react-bootstrap/Table';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { jwtDecode } from 'jwt-decode';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCloudSun, faNewspaper, faList, faLocationDot, faTemperatureThreeQuarters, faDroplet } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';

const Calendar=()=>{

  /********* Variables  *********/


  const decodeHeader = jwtDecode(localStorage.getItem('token'));
  // const formData = [decodeHeader.data.provincia, decodeHeader.data.municipio];

  const [temp, setTemp] = useState([]);
  const [max, setMax] = useState([]);
  const [min, setMin] = useState([]);
  const [hmax, setHMax] = useState([]);
  const [hmin, setHMin] = useState([]);
  const [local, setLocal] = useState([]);
  const user = decodeHeader.data.nombre;
  const [eventosParaHoje, setEventosParaHoje] = useState([]);
  const [news, setNews] = useState([]);


  useEffect(() => {

    /********* API Weather  *********/

    var url = process.env.REACT_APP_URL + '/backend/actualizaApiAemet.php';
    const formData = JSON.stringify([decodeHeader.data.provincia, decodeHeader.data.municipio]);
    axios.post(url, formData)
      .then(response => {
        setMax(response.data[0].prediccion.dia[0].temperatura.maxima); // Define os dados da resposta no estado
        setMin(response.data[0].prediccion.dia[0].temperatura.minima); 
        setHMax(response.data[0].prediccion.dia[0].humedadRelativa.maxima); 
        setHMin(response.data[0].prediccion.dia[0].humedadRelativa.minima);
        setLocal(response.data[0].nombre);
        var horaNow = Date().substring(16,18);
        console.log(response.data);
        if(horaNow <= 6) {
            setTemp(response.data[0].prediccion.dia[0].temperatura.dato[0].value); 
        } else if(horaNow <= 12) {
            setTemp(response.data[0].prediccion.dia[0].temperatura.dato[1].value); 
        } else if(horaNow  <= 18) {
            setTemp(response.data[0].prediccion.dia[0].temperatura.dato[2].value); 
        } else if(horaNow  <= 24) {
            setTemp(response.data[0].prediccion.dia[0].temperatura.dato[3].value); 
        }
        
      })
      .catch(error => {
        console.error('Error in waether:', error);

      });

    /********* API News  *********/

    url = process.env.REACT_APP_URL + '/backend/worldnewsapi.php';
    axios.post(url)
      .then(response => {
        if(response.data.status != 'failure'){
          setNews(response.data.news);
        } else {
          console.log(response.data.message);
        }
      })
      .catch(error => {
        console.error('Error in news:', error);

      });

    /********* To do list for today  *********/

    url = process.env.REACT_APP_URL + '/backend/getEventsForToday.php';
    axios.post(url, decodeHeader.data.id)
      .then((result) => {
        if(result.data.length <= 0) {
          const newResult = { time_evento: '00:00:00', texto: 'No hay evento para hoy todavia' };
          result.data.push(newResult);
        } 
        setEventosParaHoje(result.data);
      });

  }, []); 

  return(
    <div>
      <JsonLD />
      <Helmet>
        <title>Calendarfree365 - Calendario</title>
        <meta name="description" content="Página del calendario" />
        <meta name="author" content="Elton Jitiako" />
        <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="index, follow" />
        <link rel="canonical" href="calendarfree365.com" />
        <meta name="rating" content="general" />
      </Helmet>
      <Header />
      <div class="caixa">
        <h1 class="text-grey-100 text-center">CALENDARIO</h1>
        <h2 class="text-grey-100 text-center">HOLA {user}!</h2>
      <div id="dataclimabox">
      <div class="caixa1">
          <div id="databox1" class="caixa2">
              <div id="databox2" class="text-grey-100 caixa3">
                  <h2>{Date().substring(4, 7)}</h2>
                  <h1 style={{ fontSize: "200px" }}>{Date().substring(8, 10)}</h1>
                  <h2>{Date().substring(11, 15)}</h2>
              </div>
          </div>
      </div>

    <div class="caixa1">
        <div id="climabox1" class="caixa2">
            <div class="text-grey-100 caixa3">
              <Row>
                <Col lg={12} md={6} sm={6} xs={6}>
                <h3 id="climabox2"><FontAwesomeIcon icon={faCloudSun} /> CLIMA</h3>
                <h4 id="climabox2"><FontAwesomeIcon icon={faLocationDot} /> {local}</h4>
                <h3 id="climabox3">{temp}°C</h3>
                </Col>
                <Col lg={12} md={6} sm={6} xs={6}>
                <h4 id="climabox2"><FontAwesomeIcon icon={faTemperatureThreeQuarters} /> Temperatura</h4>
                <h4 id="climabox4">max: {max}°C - min: {min}°C</h4>
                <h4 id="climabox2"><FontAwesomeIcon icon={faDroplet} /> Humedad</h4>
                <h4 id="climabox4">max: {hmax}% - min: {hmin}%</h4>
                </Col>
              </Row>
              <h5 id="climabox2">FUENTE: AEMET-OPENDATA</h5>
            </div>
        </div>
    </div>
    </div>
    <div className="caixa1">
      <div className="caixa2" style={{ width: "930px" }}>
        <div className="text-grey-100 caixa3">
          <h2><FontAwesomeIcon icon={faNewspaper} /> NOTICIAS</h2>
          <Carousel interval={5000} pause={false} className="carousel-news" nextLabel="" prevLabel="" indicators={false}>
            {news.map((item, index) => (
              <Carousel.Item key={index}>
                <div className="d-flex justify-content-around align-items-center">
                  {[index === 0 ? news.length - 1 : index - 1, index, index === news.length - 1 ? 0 : index + 1].map((idx) => {
                    if (idx >= 0 && idx < news.length) {
                      const newsItem = news[idx];
                      return (
                        <a key={idx} href={newsItem.url} className="news-item" style={{ position: "relative", textDecoration: "none", display: "block" }}>
                          <img src={newsItem.image} alt="news" style={{ width: "100%", height: "100%", objectFit: "cover" }} />
                          <div className="news-overlay">
                            <h4 className="text-center">{newsItem.title}</h4>
                          </div>
                        </a>
                      );
                    } else {
                      return null;
                    }
                  })}
                </div>
              </Carousel.Item>
            ))}
          </Carousel>
          <h5>FUENTE: WORLD NEWS API</h5>
        </div>
      </div>
    </div>
    <div class="caixa1" style={{ paddingBottom: '80px', }}>
        <div class="caixa2" style={{ width: "930px", minHeigth: "900px" }}>
            <div class="text-grey-100 caixa3">
                <h2><FontAwesomeIcon icon={faList} /> EVENTOS PARA HOY</h2>
                <Table striped>
                  <thead>
                    <tr>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Horario:</th>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>Texto:</th>
                    </tr>
                  </thead>
                  <tbody>
                    {eventosParaHoje.map(evento => (
                      <tr key={evento.id}>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{evento.time_evento}</td>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{evento.texto}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
            </div>
        </div>
    </div>
    </div>
    </div>
  )
}

export default Calendar;