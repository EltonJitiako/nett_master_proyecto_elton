import Header from '../Components/Header';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSquareCheck, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';



const EventoEditar=()=>{

  /********* Variables  *********/

  let navigate = useNavigate();
  const { id } = useParams();
  const [formData, setFormData] = useState({
    id_evento: id,
    date_evento: '',
    time_evento: '',
    texto: ''
  });
  

  useEffect(() => {

    var url = process.env.REACT_APP_URL + '/backend/getEvent.php';
    axios.post(url, id)
    .then((result) => {
      document.getElementById("date").innerText =  result.data[0].date_evento;
      document.getElementById("time").innerText =  result.data[0].time_evento;
      document.getElementById("texto").placeholder =   result.data[0].texto;
    });

  }, [id]);
  
        
  const handleSubmit = async (event) => {
    event.preventDefault();
      try {
      
        var url = process.env.REACT_APP_URL + '/backend/editEvent.php';
        const formData2 = JSON.stringify(formData);
        await axios.post(url, formData2);
        navigate('/events');
        window.location.reload();
      } catch (error) {
        alert('Error editing user' + error);
      }
    
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };
  
  const handleDelete = () => {

      try {
        var url = process.env.REACT_APP_URL + '/backend/deleteEvent.php';
        axios.post(url, id);
        navigate('/events');
        window.location.reload();
      } catch (error) {
        alert('Error deleting user!' + error);
      }
    
  };
    
    return(
      <div>
        <JsonLD />
        <Helmet>
            <title>Calendarfree365 - Editar evento</title>
            <meta name="description" content="Página edición del evento" />
            <meta name="author" content="Elton Jitiako" />
            <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta name="robots" content="index, follow" />
            <link rel="canonical" href="calendarfree365.com" />
            <meta name="rating" content="general" />
        </Helmet>
        <Header />
        <div class="caixa">
          <h1 class="text-white text-center">EDITAR</h1>
          <div class="caixa1" style={{ paddingBottom: '80px', }}>
            <div class="caixa2">
              <div class="text-grey-100 caixa3">
                <h2>Editar evento {id}</h2>
                  <Form onSubmit={handleSubmit}>
                    <Row>
                      <Col md={12} lg={6}>
                        <Form.Group class="mb-3" controlId="formGroupDate">
                          <Form.Label>Fecha (<b id="date"></b>)</Form.Label>
                          <Form.Control type="date" name="date_evento" onChange={handleChange} />
                        </Form.Group>
                      </Col>
                      <Col md={12} lg={6}>
                        <Form.Group class="mb-3" controlId="formGroupTime">
                          <Form.Label>Horario(<b id="time"></b>)</Form.Label>
                          <Form.Control type="time" name="time_evento" onChange={handleChange} />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Form.Group class="mb-3" controlId="formGroupTexto">
                        <Form.Label>Texto</Form.Label>
                        <Form.Control id="texto" type="text" name="texto" placeholder="Texto" onChange={handleChange} />
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3">
                      <Col>
                        <Button variant="success" type="submit" name="submit" className="me-2"><FontAwesomeIcon icon={faSquareCheck} /> Guardar cambios</Button>
                        <Button variant="danger" onClick={handleDelete}><FontAwesomeIcon icon={faTrash} /> Borrar evento</Button>
                      </Col>
                    </Form.Group>
                  </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}

export default EventoEditar;