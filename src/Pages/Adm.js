
import Header from '../Components/Header';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import axios from 'axios';
import { useState } from 'react';
import { useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPenToSquare, faSquareCheck } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';


const Adm=()=>{

  /********* Variables  *********/

  const [formData, setFormData] = useState({
    maintext: '',
    image: ''
  });
  const [logsErrores, setLogsErrores] = useState('');
  const [mainTextPlaceholder, setMainTextPlaceholder] = useState('');
  const [imagenPlaceholder, setImagenPlaceholder] = useState('');
  const [users, setUsers] = useState([]);

  /********* Edit Home  *********/

  const handleSubmit = async (event) => {
    event.preventDefault();
      try {
        var url = process.env.REACT_APP_URL + '/backend/editHome.php';
        const formData2 = JSON.stringify(formData);
        await axios.post(url, formData2);
        //window.location.reload();
      } catch (error) {
        alert('Error editing user!' + error);
      }
    
  };
  
  /********* Form Data *********/

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
      setFormData((prevData) => {
        const newData = Object.assign({}, prevData);
        newData[name] = value;
        return newData;
      });


  };
    
  useEffect(() => {

    var url = process.env.REACT_APP_URL + '/backend/getHome.php';
    axios.post(url)
    .then((result) => {
      setMainTextPlaceholder(result.data[0].maintext);
      setImagenPlaceholder(result.data[0].imagen);
    });

  
    url = process.env.REACT_APP_URL + '/backend/getUsers.php';
    axios.post(url)
    .then((result) => {
      setUsers(result.data);
    });
          
    const fetchLogsErrores = async () => {
        try {
            const response = await axios.get( process.env.REACT_APP_URL  + `/backend/logs/errosMysql.txt`);
            setLogsErrores(response.data);
        } catch (error) {
            console.error('Error getting errosMysql:', error);
        }
    };
      
    fetchLogsErrores();

  }, []);

 
  return(
    <div>
      <JsonLD />
      <Helmet>
        <title>Calendarfree365 - Administracion</title>
        <meta name="description" content="Página de admnistración" />
        <meta name="author" content="Elton Jitiako" />
        <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="index, follow" />
        <link rel="canonical" href="URL_canônica" />
        <meta name="rating" content="general" />
      </Helmet>
      <Header />
      <div class="caixa">
        <h1 class="text-grey-100 text-center">ADMINISTRACION</h1>
        <div class="caixa1">
          <div class="caixa2">
            <div class="text-grey-100 caixa3">
              <h2>Pagina Home</h2>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Texto central</Form.Label>
                      <Form.Control id="maintext" type="text" name="maintext" placeholder={mainTextPlaceholder} onChange={handleInputChange} />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Imagen GIF</Form.Label>
                      <Form.Control id="image" type="text" name="image" placeholder={imagenPlaceholder} onChange={handleInputChange} />
                    </Form.Group>
                  </Col>
                </Row>
                <Form.Group as={Row} className="mb-3">
                    <Col>
                      <Button variant="success" type="submit" name="submit"><FontAwesomeIcon icon={faSquareCheck} /> Guardar cambios</Button>
                    </Col>
                </Form.Group>
              </Form>
            </div>
          </div>
        </div>
        <div class="caixa1">
            <div class="caixa2">
                <div class="text-grey-100 caixa3">
                    <h2>Usuarios</h2>
                    <div style={{ overflowX: 'auto', maxWidth: '100%'}}>
                      <Table id="tabelaUsuarios" striped="columns" style={{ width: '100%', borderCollapse: 'collapse' }}>
                        <thead>
                          <tr>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Id:</th>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>Email:</th>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Nombre:</th>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>Apellido:</th>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Provincia:</th>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>Municipio:</th>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Rol:</th>
                            <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>Editar:</th>
                          </tr>
                        </thead>
                        <tbody>
                          {users.map(user => (
                            <tr key={user.id}>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{user.id}</td>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{user.email}</td>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{user.nombre}</td>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{user.apellido}</td>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{user.provincia}</td>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{user.municipio}</td>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}>{user.rol}</td>
                              <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7" }}><a style={{textDecoration: 'none'}} href={process.env.REACT_APP_DATABASE + "/#/user/" + user.id}><FontAwesomeIcon icon={faPenToSquare} /></a></td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                    </div>
                </div>
            </div>
        </div>
        <div class="caixa1" style={{ paddingBottom: '80px', }}>
            <div class="caixa2">
                <div class="text-grey-100 caixa3">
                  <h2>Logs Errores</h2>
                  <pre>{logsErrores}</pre>
                </div>
            </div>
        </div>
      </div>
    </div>

  )
}

export default Adm;