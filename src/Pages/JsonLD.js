import React from 'react';

function JsonLD() {
    return (
        <script type="application/ld+json">
            {`
                {
                    "@context": "https://schema.org",
                    "@type": "Organization",
                    "name": "Calendarfree365",
                    "url": "https://calendarfree365.com",
                    "logo": "https://vigilant-bell.82-223-152-234.plesk.page/static/media/logo.caaeccc4a4057497c1b8.png",
                    "description": "Un calendario sencillo, moderno y gratuito para siempre"
                }
            `}
        </script>
    );
}

export default JsonLD;