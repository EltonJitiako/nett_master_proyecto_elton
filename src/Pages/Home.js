import pic0 from '../img/giflogo.gif';
import Header from '../Components/Header';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';
import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';


const Home=()=>{

  /********* Variables  *********/

  const [mainTextPlaceholder, setMainTextPlaceholder] = useState('');
  const [imagenPlaceholder, setImagenPlaceholder] = useState('');

  /********* Get Home  *********/

  useEffect(() => {
    var url = process.env.REACT_APP_URL + '/backend/getHome.php';
    axios.post(url)
    .then((result) => {
      setMainTextPlaceholder(result.data[0].maintext);
      setImagenPlaceholder(result.data[0].imagen);
    });
  }, []);

    return(
    <div>
      <JsonLD />
      <Helmet>
        <title>Calendarfree365 - Home</title>
        <meta name="description" content="Página principal (home)" />
        <meta name="author" content="Elton Jitiako" />
        <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="index, follow" />
        <link rel="canonical" href="calendarfree365.com" />
        <meta name="rating" content="general" />
      </Helmet>
      <Header />
      <body>
        <div class="caixa">
          <div class="caixa1" style={{ paddingBottom: '80px', }}>
            <div class="caixa2">
              <div class="text-grey-100 caixa3">
                <Row className="align-item-center">
                  <Col md={12} lg={6} style={{ width: '400px',height: 'auto' }}>
                    <p class="texto">¡CALENDARIO SENCILLO Y GRATUITO!</p>
                    <p class="texto2">{mainTextPlaceholder}</p>
                    <Link class="nav-link navbar-brand" to='/Register'>
                      <Button type="submit" name="submit" variant="primary">Registrar aquí</Button>
                      </Link>
                    <br></br>
                  </Col>
                  <Col md={12} lg={6} style={{ width: '400px',height: 'auto' }}>
                    <img id="img1" style={{ width: '100%',height: '100%' }} src={pic0} alt="main pic"/>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        </div>
      </body>
    </div>

    )
}

export default Home;