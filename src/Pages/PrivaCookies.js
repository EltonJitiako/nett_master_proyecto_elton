import Header from '../Components/Header';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';

const PrivaCookies=()=>{
    return(
        <div>
            <JsonLD />
            <Helmet>
                <title>Calendarfree365 - Privacidad y Cookies</title>
                <meta name="description" content="Página privacidad y cookies" />
                <meta name="author" content="Elton Jitiako" />
                <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="robots" content="index, follow" />
                <link rel="canonical" href="calendarfree365.com" />
                <meta name="rating" content="general" />
            </Helmet>
            <Header />
            <div class="caixa">
                <div class="caixa1" style={{ paddingBottom: '80px', }}>
                    <div class="caixa2">
                        <div class="text-grey-100 caixa3">
                            <div>
                                <h1>Política de Privacidad y Cookies</h1>
                                <h2>Toda la información que solicitamos en el formulario de registro la utilizaremos exclusivamente para el procesamiento e identificación del usuario. La identificación del usuario que requerimos en el formulario es voluntaria. Estos datos pasarán a formar parte del fichero “Users”.
Este fichero es propiedad de Calendarfree365, entidad a la que podrá dirigirse en cualquier momento para ejercitar sus derechos de acceso, rectificación, cancelación u oposición o solicitar cualquier otra información. Puedes ejercer estos derechos escribiendo a eltonjitiako@gmail.com, en cumplimiento con la Ley de Protección de Datos de Carácter Personal.</h2>
                                <h2>Hasta este momento esta pagina web no utiliza de cookies para guardar datos de los usuarios.</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PrivaCookies;