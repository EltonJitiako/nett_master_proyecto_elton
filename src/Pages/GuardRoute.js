import React from 'react';
import ErrorPage from './ErrorPage';


const GuardRoute = ({ children }) => {
    const token = localStorage.getItem('token');

    if(token) {
        return <>{ children }</>
    } else {
        return <>{<ErrorPage />}</>
    }

}

export default GuardRoute;