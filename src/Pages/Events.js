import { useState } from 'react';
import axios from 'axios';
import Header from '../Components/Header';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Table from 'react-bootstrap/Table';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { jwtDecode } from 'jwt-decode';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPenToSquare, faPlus, faSquareCheck } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';

function MyVerticallyCenteredModal(props) {

  /********* Variables  *********/

  const decodeHeader = jwtDecode(localStorage.getItem('token'));
  const [eventsAll, setEventsAll] = useState([]);
  const [formData, setFormData] = useState({
    id: decodeHeader.data.id,
    date: '',
    time: '',
    texto: ''
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      console.log("teste", formData.id);
      var url = process.env.REACT_APP_URL + '/backend/insertEvent.php';
      const formData2 = JSON.stringify(formData);
      const response = await axios.post(url, formData2);
      window.location.reload();
    } catch (error) {
      alert('Error ao inserir usuario!' + error);
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Adicionar evento
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Row>
            <Col md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formGroupDate">
                <Form.Label>Fecha</Form.Label>
                <Form.Control type="date" name="date" onChange={handleChange} />
              </Form.Group>
            </Col>
            <Col md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formGroupTime">
                <Form.Label>Horario</Form.Label>
                <Form.Control type="time" name="time" onChange={handleChange} />
              </Form.Group>
            </Col>
          </Row>
            <Form.Group className="mb-3" controlId="formGroupTexto">
                <Form.Label>Texto</Form.Label>
                <Form.Control type="text" name="texto" placeholder="Texto" onChange={handleChange} />
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
                <Col>
                  <Button type="submit" name="submit" variant="success" onClick={props.onHide}><FontAwesomeIcon icon={faSquareCheck} /> Guardar</Button>
                </Col>
              </Form.Group>
        </Form>
      </Modal.Body>
    </Modal>
  );
}


const Events=()=>{

    /********* Variables  *********/

  const decodeHeader = jwtDecode(localStorage.getItem('token'));
  const [eventsAll, setEventsAll] = useState([]);
  const [modalShow, setModalShow] = useState(false); 

    
      
  useEffect(() => {
    var url = process.env.REACT_APP_URL + '/backend/getEvents.php';
          axios.post(url, decodeHeader.data.id)
          .then((result) => {
            if(result.data.length <= 0) {
              const newResult = { time_evento: '00:00:00', texto: 'No hay evento para este usuario todavia' };
              result.data.push(newResult);
            } 
            setEventsAll(result.data);
          });

  }, []);  

    
  return (
    <div>
    <JsonLD />
    <Helmet>
        <title>Calendarfree365 - Evento</title>
        <meta name="description" content="Página de eventos" />
        <meta name="author" content="Elton Jitiako" />
        <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="index, follow" />
        <link rel="canonical" href="calendarfree365.com" />
        <meta name="rating" content="general" />
    </Helmet>
    <Header />
    <div class="caixa">
      <h1 class="text-white text-center">EVENTOS</h1>
        <div class="caixa1" style={{ paddingBottom: '80px', }}>
          <div class="caixa2">
            <div class="text-grey-100 caixa3">
              <Button className="mb-3" type="submit" name="submit" variant="primary" onClick={setModalShow}><FontAwesomeIcon icon={faPlus} /> Adicionar Evento</Button>
                <MyVerticallyCenteredModal
                    show={modalShow}
                    onHide={() => setModalShow(false)}
                />
                <div style={{ overflowX: 'auto', maxWidth: '100%'}}>
                <Table striped  style={{ width: '100%', borderCollapse: 'collapse' }}>
                  <thead>
                    <tr>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>ID Evento:</th>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>ID Usuario:</th>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Texto:</th>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Fecha:</th>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Hora:</th>
                      <th style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>Editar:</th>
                    </tr>
                  </thead>
                  <tbody>
                    {eventsAll.map(event => (
                      <tr key={event.id}>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>{event.id_evento}</td>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>{event.id}</td>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>{event.texto}</td>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>{event.date_evento}</td>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}>{event.time_evento}</td>
                        <td style={{ backgroundColor: "rgba(243, 235, 235, 0.14)", color: "#EDF2F7"}}><a style={{textDecoration: 'none'}} href={process.env.REACT_APP_DATABASE + "/#/event/" + event.id_evento}><FontAwesomeIcon icon={faPenToSquare} /></a></td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
    
    
}

export default Events;