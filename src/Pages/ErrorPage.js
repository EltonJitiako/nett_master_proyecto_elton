import gatoPerdido from '../img/gatoPerdido.gif';
import Header from '../Components/Header';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';

const ErrorPage=()=>{
    return(
        <div>
            <JsonLD />
            <Helmet>
                <title>Calendarfree365 - Error</title>
                <meta name="description" content="Página de error" />
                <meta name="author" content="Elton Jitiako" />
                <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="robots" content="index, follow" />
                <link rel="canonical" href="calendarfree365.com" />
                <meta name="rating" content="general" />
            </Helmet>
            <Header />
            <div class="caixa">
                <div class="caixa1" style={{ paddingBottom: '80px', }}>
                    <div class="caixa2">
                        <div class="text-grey-100 caixa3">
                            <div>
                                <h1 style={{ width: '400px',height: 'auto' }}>¡¡¡Estoy perdido!!!</h1>
                                <img id="img1" style={{ width: '100%',height: '100%' }} src={gatoPerdido} alt="gatoPerdido"></img>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ErrorPage;