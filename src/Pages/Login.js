import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import '../App.css';
import Header from '../Components/Header';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import sky from '../img/sky.gif';
import { jwtDecode } from 'jwt-decode';
import { Helmet } from 'react-helmet';
import JsonLD from './JsonLD';

const Login=()=>{

  /********* Variables  *********/

  let navigate = useNavigate();
  const [token, setToken] = useState(localStorage.getItem('token') || '');
  const [user, setUser]=useState({email:'',contrasena:''});

  /********* Check if already have a token  *********/

  useEffect(() => {

    if(token != '') {
      navigate('/Calendar');
    }

  }, []); 
    

  const handleChange=(e)=>{
      setUser({...user, [e.target.name]: e.target.value})
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    const sendData = [];
    (async () => {
      try {
        const hash = await sha256(user.contrasena);
        const sendData = JSON.stringify({
          email:user.email,
          contrasena: hash
        });
        const url = process.env.REACT_APP_URL + '/backend/login.php';
        axios.post(url, sendData)
        .then((result) => {
          console.log(result);
          if(result.data != 'error') {
            const decodeHeader = jwtDecode(result.data.token);
            const { token } = result.data;
            window.localStorage.setItem('token', token);
            setToken(token);
            navigate('/Calendar');
          } else {
            alert('User not found');
            navigate('/register');
          }


        });
      } catch (error) {
        console.error('Erro calculating hash:', error);
      }
    })();


  };

  // Função para calcular o hash SHA-256 de uma string
  async function sha256(message) {
    const encoder = new TextEncoder();
    const data = encoder.encode(message);
    const hashBuffer = await crypto.subtle.digest('SHA-256', data);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray.map(byte => byte.toString(16).padStart(2, '0')).join('');

    return hashHex;
  };

  return(
    <div>
      <JsonLD />
      <Helmet>
        <title>Calendarfree365 - Login</title>
        <meta name="description" content="Página de login" />
        <meta name="author" content="Elton Jitiako" />
        <meta name="keywords" content="calendario, calendar, clima, weather, noticias, news" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="index, follow" />
        <link rel="canonical" href="calendarfree365.com" />
        <meta name="rating" content="general" />
      </Helmet>
      <Header />
      <div class="caixa">
        <div class="caixa1" style={{ paddingBottom: '80px', }}>
          <div class="caixa2">
            <div class="text-grey-100 caixa3">
              <Row>
                <Col md={12} lg={6} style={{ width: '400px',height: 'auto' }}>
                  <h1>Login</h1>
                  <br></br>
                  <Form onSubmit={handleSubmit}>
                      <Form.Group className="mb-3" controlId="formGroupEmail">
                          <Form.Label>Email address</Form.Label>
                          <Form.Control type="email" name="email" placeholder="Enter email" onChange={handleChange} />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formGroupPassword">
                          <Form.Label>Password</Form.Label>
                          <Form.Control type="password" name="contrasena" placeholder="Password" onChange={handleChange} />
                      </Form.Group>
                      <Form.Group as={Row} className="mb-3">
                          <Col>
                          <Button type="submit" name="submit" variant="danger">Login</Button>
                          </Col>
                      </Form.Group>
                  </Form>
                </Col>
                <Col md={12} lg={6} style={{ width: '400px',height: 'auto' }}>
                  <img id="img1" style={{ width: '100%',height: '100%' }} src={sky} alt="sky"/>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Login;