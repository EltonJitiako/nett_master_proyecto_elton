import './App.css';
import Footer from './Components/Footer';
import Home from './Pages/Home';
import Login from './Pages/Login';
import Register from './Pages/Register';
import ErrorPage from './Pages/ErrorPage';
import PrivaCookies from './Pages/PrivaCookies';
import Calendar from './Pages/Calendar';
import Adm from './Pages/Adm';
import Events from './Pages/Events';
import Perfil from './Pages/Perfil';
import UserEdit from './Pages/UserEdit';
import EventEdit from './Pages/EventEdit';
import { Route, HashRouter, Routes } from 'react-router-dom';
import GuardRoute from './Pages/GuardRoute';

function App() {
  return (
    <HashRouter>
      
    <div class="containerBox">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/*" element={<ErrorPage />} />
        <Route path="/privacookies" element={<PrivaCookies />} />
        <Route path="/calendar" element={<GuardRoute><Calendar /></GuardRoute>} />
        <Route path="/adm" element={<GuardRoute><Adm /></GuardRoute>} />
        <Route path="/events" element={<GuardRoute><Events /></GuardRoute>} />
        <Route path="/perfil" element={<GuardRoute><Perfil /></GuardRoute>} />
        <Route path="/user/:id" element={<GuardRoute><UserEdit /></GuardRoute>} />
        <Route path="/event/:id" element={<GuardRoute><EventEdit /></GuardRoute>} />
      </Routes>
      <Footer />
    </div>

    </HashRouter>
  );
}

export default App;
